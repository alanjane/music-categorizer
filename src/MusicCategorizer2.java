import java.awt.*;
import java.io.*;
import javax.swing.*;
import java.util.*;

public class MusicCategorizer2 {

    // GUI
    private JFrame f_mainFrame;
    private BorderLayout mainFrameLayout;
    private JList ui_songsList;

    // Variables
    private String musicRootPath;

    private MusicCategorizer2() {
        // TODO: Allow user to chose path.
        musicRootPath = "/home/alex/Music/music_test";

        f_mainFrame = new JFrame("Music Categorizer");
        f_mainFrame.setSize(800, 500);
        f_mainFrame.setLocationRelativeTo(null);
        f_mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainFrameLayout = new BorderLayout();
        f_mainFrame.setLayout(mainFrameLayout);

        ui_songsList = new JList();
        ui_songsList.setFixedCellWidth(200);
        ArrayList<String> a = new ArrayList<String>();
        a.add("one");
        a.add("two");
        a.add("one");
        a.add("two");
        a.add("one");
        a.add("two");
        JScrollPane ui_songListScroll = new JScrollPane();
        String[] sl = getListOfSongsInFolder(musicRootPath);
        ui_songsList.setListData(sl);
        ui_songsList.setLayoutOrientation(JList.VERTICAL);
        ui_songListScroll.setViewportView(ui_songsList);
        
        f_mainFrame.add(ui_songListScroll, BorderLayout.WEST);

        f_mainFrame.setVisible(true);
    }

    private String[] getListOfSongsInFolder(String path) {
        // TODO: Access songs in subfolders, too.
        File musicFolder = new File(path);
        String musicFolderContents[] = musicFolder.list();

        ArrayList<String> tempSongPaths = new ArrayList<String>();
        for (String filename : musicFolderContents)
            if (filename.length() > 3 && filename.substring(filename.length() - 4).equals(".mp3"))
                tempSongPaths.add(filename);

        return tempSongPaths.toArray(new String[0]);
    }

    public static void main(String[] args) {
        new MusicCategorizer2();
    }
}
